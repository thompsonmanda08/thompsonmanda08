<img align="right" src="https://visitor-badge.laobi.icu/badge?page_id=thompsonmanda08.thompsonmanda08&"  />

###

<br clear="both">

<h1 align="left">Hi 👋! Thompson Here!</h1>

###

<p align="left">From self-taught to well-rounded: Navigating the ever-evolving landscape of software engineering! 🚀 Frontend enthusiast on a mission to learn, adapt, and embrace the endless possibilities of coding. 💻✨ #Interwebb #WeGoAgain #CodingAdventure #NeverStopLearning</p>

###

<h3 align="left">I'm happy to collaborate with you; find me here</h3>

###

<div align="left">
  <a href="mailto:thompsonmanda80@gmail.com" target="_blank">
    <img src="https://img.shields.io/static/v1?message=Gmail&logo=gmail&label=&color=D14836&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="gmail logo"  />
  </a>
  <a href="https://linkedin.com/in/thompsonmanda08" target="_blank">
    <img src="https://img.shields.io/static/v1?message=LinkedIn&logo=linkedin&label=&color=0077B5&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="linkedin logo"  />
  </a>
  <a href="blackcruz08" target="_blank">
    <img src="https://img.shields.io/static/v1?message=Discord&logo=discord&label=&color=7289DA&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="discord logo"  />
  </a>
  <a href="https://www.behance.net/thompsonmanda08" target="_blank">
    <img src="https://img.shields.io/static/v1?message=Behance&logo=behance&label=&color=1769ff&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="behance logo"  />
  </a>
  <a href="https://codepen.io/thompsonmanda08" target="_blank">
    <img src="https://img.shields.io/static/v1?message=Codepen&logo=codepen&label=&color=000000&logoColor=white&labelColor=&style=for-the-badge" height="35" alt="codepen logo"  />
  </a>
</div>

###

<h3 align="left">Design Tool Kit</h3>

###

<div align="left">
  <img src="https://img.shields.io/badge/Figma-F24E1E?logo=figma&logoColor=white&style=for-the-badge" height="30" alt="figma logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Adobe XD-FF61F6?logo=adobexd&logoColor=black&style=for-the-badge" height="30" alt="xd logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Adobe Illustrator-FF9A00?logo=adobeillustrator&logoColor=black&style=for-the-badge" height="30" alt="adobeillustrator logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Adobe Photoshop-31A8FF?logo=adobephotoshop&logoColor=black&style=for-the-badge" height="30" alt="adobephotoshop logo"  />
</div>

###

<h3 align="left">Web/Mobile App Frontend Technologies</h3>

###

<div align="left">
  <img src="https://img.shields.io/badge/HTML5-E34F26?logo=html5&logoColor=white&style=for-the-badge" height="30" alt="html5 logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/CSS3-1572B6?logo=css3&logoColor=white&style=for-the-badge" height="30" alt="css3 logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/JavaScript-F7DF1E?logo=javascript&logoColor=black&style=for-the-badge" height="30" alt="javascript logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/React-61DAFB?logo=react&logoColor=black&style=for-the-badge" height="30" alt="react logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Next.js-000000?logo=nextdotjs&logoColor=white&style=for-the-badge" height="30" alt="nextjs logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/TypeScript-3178C6?logo=typescript&logoColor=white&style=for-the-badge" height="30" alt="typescript logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Tailwind CSS-06B6D4?logo=tailwindcss&logoColor=black&style=for-the-badge" height="30" alt="tailwindcss logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Flutter-02569B?logo=flutter&logoColor=white&style=for-the-badge" height="30" alt="flutter logo"  />
  <img width="12" />
  <img src="https://img.shields.io/badge/Dart-0175C2?logo=dart&logoColor=white&style=for-the-badge" height="30" alt="dart logo"  />
</div>

###

<h3 align="left">Backend Dev, Version Control, DevOps, IDE Toolkit</h3>

###

<div align="left">
  <img src="https://skillicons.dev/icons?i=nodejs" height="30" alt="nodejs logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/dart/dart-original.svg" height="30" alt="dart logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" height="30" alt="python logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/postgresql/postgresql-original.svg" height="30" alt="postgresql logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/mysql/mysql-original.svg" height="30" alt="mysql logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=django" height="30" alt="django logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=gitlab" height="30" alt="gitlab logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=github" height="30" alt="github logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=git" height="30" alt="git logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=figma" height="30" alt="figma logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=redux" height="30" alt="redux logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=sass" height="30" alt="sass logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=vscode" height="30" alt="vscode logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/visualstudio/visualstudio-plain.svg" height="30" alt="visualstudio logo"  />
  <img width="12" />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/yarn/yarn-original.svg" height="30" alt="yarn logo"  />
  <img width="12" />
  <img src="https://cdn.simpleicons.org/postman/FF6C37" height="30" alt="postman logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=tailwind" height="30" alt="tailwindcss logo"  />
  <img width="12" />
  <img src="https://cdn.simpleicons.org/vite/646CFF" height="30" alt="vite logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=ai" height="30" alt="adobeillustrator logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=ps" height="30" alt="adobephotoshop logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=bootstrap" height="30" alt="bootstrap logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=js" height="30" alt="javascript logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=prisma" height="30" alt="prisma logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=react" height="30" alt="react logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=ts" height="30" alt="typescript logo"  />
  <img width="12" />
  <img src="https://skillicons.dev/icons?i=linux" height="30" alt="linux logo"  />
</div>

###

<div align="center">
  <img src="https://github-readme-stats.vercel.app/api?username=thompsonmanda08&hide_title=false&hide_rank=false&show_icons=true&include_all_commits=true&count_private=true&disable_animations=false&theme=tokyonight&locale=en&hide_border=true&order=1" height="150" alt="stats graph"  />
  <img src="https://github-readme-stats.vercel.app/api/top-langs?username=thompsonmanda08&locale=en&hide_title=false&layout=compact&card_width=320&langs_count=6&theme=tokyonight&hide_border=true&order=2" height="150" alt="languages graph"  />
  <img src="https://streak-stats.demolab.com?user=thompsonmanda08&locale=en&mode=weekly&theme=tokyonight&hide_border=true&border_radius=5&order=3" height="150" alt="streak graph"  />
</div>

###

  [![An image of @thompsonmanda08's Holopin badges, which is a link to view their full Holopin profile](https://holopin.me/thompsonmanda08)](https://holopin.io/@thompsonmanda08)


